

<?php
   header('Access-Control-Allow-Origin:*');

   // Define database connection parameters
   $hn      = 'localhost';
   $un      = 'root';
   $pwd     = '';
   $db      = 'klubus';
   $cs      = 'utf8';

   // Set up the PDO parameters
   $dsn 	= "mysql:host=" . $hn . ";port=3306;dbname=" . $db . ";charset=" . $cs;
   $opt 	= array(
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                        PDO::ATTR_EMULATE_PREPARES   => false,
                       );
   // Create a PDO instance (connect to the database)
   $pdo 	= new PDO($dsn, $un, $pwd, $opt);
   $data    = array();

      $latitudes		     = filter_var($obj->latitude,FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
         $longitudes	  = filter_var($obj->longitude, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);


$rad = 10; // radius of bounding circle in kilometers

    $R = 6371;  // earth's mean radius, km

    // first-cut bounding box (in degrees)
    $maxLat = $latitudes + rad2deg($rad/$R);
    $minLat = $latitudes - rad2deg($rad/$R);
    $maxLon = $longitudes + rad2deg(asin($rad/$R) / cos(deg2rad($latitudes)));
    $minLon = $longitudes- rad2deg(asin($rad/$R) / cos(deg2rad($latitudes)));
   // Attempt to query database table and retrieve data
   try {
     // $stmt 	= $pdo->query("SELECT LATITUDE, LONGITUDE FROM routes WHERE sno IN (SELECT MAX(sno) FROM routes WHERE LATITUDE BETWEEN '$maxLat' AND '$minLat' AND LONGITUDE BETWEEN  '$maxLon' AND '$minLon')");
     // $stmt 	= $pdo->query("SELECT LATITUDE, LONGITUDE FROM routes WHERE sno=='1'")
$stmt 	= $pdo->query('SELECT LATITUDE, LONGITUDE FROM routes WHERE sno IN (SELECT MAX(sno) FROM routes WHERE LATITUDE 
BETWEEN '$maxLat' AND '$minLat' AND LONGITUDE BETWEEN '$maxLon' AND '$minLon')');
while($row  = $stmt->fetch(PDO::FETCH_OBJ))
      {
         // Assign each row of data to associative array
         $data[] = $row;
      }

      // Return data as JSON
      echo json_encode($data);
   }
   catch(PDOException $e)
   {
      echo $e->getMessage();
   }

?> 
